## INSTALLATION

To create a docker image create first the .jar file.


### CREATE A JAR FILE
downloads all dependencies and creates a .jar file in "./target" directory
`mvn clean compile assembly:single`

### CREATE A DOCKER IMAGE
to build the image
    `sudo docker build -t kebapp .`
to run the app and connect it to the host'ss network stack
    `sudo docker run --network=host kebapp`



## LINKS
[Docker +mvn tutorial](https://blog.giantswarm.io/getting-started-with-java-development-on-docker/)
