#FROM java:8 
FROM hypriot/rpi-java

# Install maven
#RUN apt-get update  
#RUN apt-get install -y maven

WORKDIR /code

# Prepare by downloading dependencies
ADD pom.xml /code/pom.xml  
#RUN ["mvn", "dependency:resolve"]  
#RUN ["mvn", "verify"]

# Adding source, compile and package into a fat jar
ADD src /code/src  
ADD target /code/target
#RUN ["mvn", "clean", "compile", "assembly:single"]

EXPOSE 4567  
CMD ["java", "-cp", "target/kebapp-0.0.1-SNAPSHOT-jar-with-dependencies.jar", "ucl.kebapp.RouteFinder"]  
